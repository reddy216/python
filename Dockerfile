FROM python:latest 

RUN pip install django
COPY app /pythonproject
WORKDIR /pythonproject
EXPOSE 8000

ENTRYPOINT ["python","manage.py","runserver","0.0.0.0:8000"]
